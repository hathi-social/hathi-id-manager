#!/bin/bash

psql -c 'DROP DATABASE hathi_id_mgr_unittest' -c 'CREATE DATABASE hathi_id_mgr_unittest' postgres &&
psql -h localhost -f ../db/ddl.sql -f ../db/savesession.sql -f ../tests/unit/pgsqldb.sql hathi_id_mgr_unittest hathi_unittest
