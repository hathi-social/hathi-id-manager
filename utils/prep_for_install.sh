#!/bin/bash

psql -c 'CREATE DATABASE hathi_id_mgr' postgres &&
psql -h localhost -f ../db/ddl.sql -f ../db/savesession.sql -f ../db/acl.sql hathi_id_mgr
