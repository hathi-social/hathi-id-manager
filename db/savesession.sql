CREATE OR REPLACE FUNCTION savesession (name_param VARCHAR(255), session_param BYTEA) RETURNS VOID AS
$$
DECLARE
    tgt_key TEXT;
BEGIN
    -- Handle inserting new session, or refreshing existing session
    INSERT INTO hathi_sessions(name, session) 
        VALUES (name_param, session_param)
        ON CONFLICT ON CONSTRAINT hathi_sessions_key DO UPDATE SET
        lastused = NOW();
EXCEPTION WHEN unique_violation THEN
    BEGIN
        GET STACKED DIAGNOSTICS tgt_key = CONSTRAINT_NAME;
        RAISE LOG 'Value of tgt_key: %', tgt_key;
        -- Handle replacing username's session key
        IF tgt_key = 'hathi_sessions_name_key' THEN
            RAISE LOG 'Updating % with new session key', name_param;
            UPDATE hathi_sessions SET session = session_param,
                lastused = DEFAULT
                WHERE name = name_param;
        ELSE
            -- Handle duplicate session key
            RAISE unique_violation USING CONSTRAINT = tgt_key;
        END IF;
    END;
END
$$
LANGUAGE plpgsql;
