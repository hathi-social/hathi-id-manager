/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

///
/// Generate a salted hash outputted in format that can be used in a C string array
///

#include <iostream>
#include <sstream>
#include <iomanip>

#include <log4cplus/configurator.h>

#include "idmgr-version.h"
#include "password.h"

using namespace hathi_id_manager;

void hexEncode(const string &buf)
{
    bool isFirst = true;
    for(unsigned char i : buf)
    {
        cout << ((isFirst) ? "" : ",")
            << "0x" << setw(2) << setfill('0') << hex << (unsigned int)i;
        isFirst = false;
    }
}

int main() 
{
    log4cplus::initialize();
    log4cplus::BasicConfigurator config;
    config.configure();

    cout << "Enter password to hash: ";
    string input;
    getline(cin, input);
    cout << endl << "You entered: " << input << endl;

    Password p;
    auto rslt = p.calcPassHash(input);
    cout << "Hashed password:"<<endl;
    hexEncode(p.getHash(rslt));
    cout << endl << "Salt:"<<endl;
    hexEncode(p.getSalt(rslt));
    cout << endl << "Mode: " << (unsigned int)p.getHashMode(rslt) << endl;

    return 0;
}
