/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <tuple>
#include <string>
#include <memory>
#include <functional>
#include <optional>
#include <array>
#include <stdexcept>

#include <log4cplus/logger.h>

#include <postgresql/libpq-fe.h>
#include <gtest/gtest_prod.h>

#include "db.h"

namespace hathi_id_manager
{

///
/// PostgreSQL implementation of DB class
///
class PgsqlDB : public DB
{
private:
    PgsqlDB() : port(0), timeout(0)
    {}

    PgsqlDB(const PgsqlDB &cpy) = default;
    PgsqlDB(PgsqlDB &&cpy) = default;
    PgsqlDB & operator = (const PgsqlDB &rhs) = default;
    PgsqlDB & operator = (PgsqlDB &&rhs) = default;

    log4cplus::Logger logger =
        log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("PgsqlDB"));

    std::string host;
    unsigned int port;
    std::string username, dbName;

    // A timeout longer than 30 seconds will probably affect user experience
    // of the whole system.
    unsigned short timeout;

    std::unique_ptr<PGconn, std::function<void(PGconn *)>> conn = nullptr;

    enum SocketState : int {
        UNSET,
        WRITEABLE,
        READABLE,
        BOTH,
        TIMEOUT
    };

    ///
    /// Handle async-polling of the PGconn socket
    ///
    const SocketState pollSocket(const int socket, const SocketState &watchState) const;

    ///
    /// Wait for DB socket to be writable for sending query
    ///
    void waitDBWriteable() const;

    ///
    /// Wait for data from DB server
    ///
    void waitResultReady() const;

    ///
    /// Send query to DB
    ///
    template<size_t N>
    void queryDB(const std::string &query,
        const std::array<const std::string, N> &params) const;

    ///
    /// Convert escaped BYTEA to binary
    ///
    /// \param escapedData PostgreSQL hex-escaped data string
    ///
    /// \return std::string containing the binary data
    ///
    const std::string makeRawBinary(const char *escapedData) const;

    ///
    /// Escape bytea data
    ///
    const std::string escapeBinary(const std::string &rawBin) const;

    ///
    /// Flush any unprocessed received data
    ///
    void flushResponses() const;

    ///
    /// PGresult deleter
    ///
    class CleanPGresult
    {
    public:
        void operator()(PGresult *p)
        {
            PQclear(p);
        }
    };

    inline void checkConnected() const
    {
        if(!conn)
            throw std::logic_error("Attempting query while disconnected");
    }

public:
    ///
    /// Constructor
    ///
    /// \param host DB hostname
    /// \param DB port
    /// \param username Username to login to the DB server
    /// \param dbName Database to use
    /// \param timeout Number of seconds to timeout a DB operation. Default to 10
    ///
    PgsqlDB(const std::string &host,
        const unsigned int &port,
        const std::string &username,
        const std::string &dbName,
        const unsigned short &timeout = 10) : host(host),
            port(port),
            username(username),
            dbName(dbName),
            timeout(timeout)
    {}

    virtual ~PgsqlDB(){}

    ///
    /// Implmenent DB::connect
    ///
    virtual void connect() override;

    ///
    /// Implement DB::addUser
    ///
    virtual void addActor(const std::string &name,
        const std::string &hashedPass, const std::string &salt,
        const unsigned char &hashMode) const override;

    ///
    /// Implement DB::getActorPass
    ///
    virtual const std::optional<const password> getActorPass(
        const std::string &name) const override;

    ///
    /// Implement DB::checkSession
    ///
    virtual std::optional<std::string> checkSession(
        const std::string &session,
        const unsigned int &timeout) const override;

    ///
    /// Implement DB::saveSession
    ///
    virtual const bool saveSession(
        const std::string &username,
        const std::string &session) const override;

    ///
    /// Implement DB::deleteSession
    ///
    virtual const bool deleteSession(const std::string &session) const override;

    ///
    /// Implement DB::deleteActor
    ///
    virtual const bool deleteActor(const std::string &username) const override;

    FRIEND_TEST(PgsqlDBTest, connectSuccess);
    FRIEND_TEST(PgsqlDBTest, connectFail);
};

} //namespace
