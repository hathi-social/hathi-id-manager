/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <functional>
#include <openssl/ssl.h>

#include <log4cplus/logger.h>

#include "socketinfo.h"
#include "clienthandler.h"

namespace hathi_id_manager
{

/// 
/// Server-side socket connection
///
class ServerSocket : public SocketInfo
{
public:
    ServerSocket() : logger(log4cplus::Logger::getInstance("ServerSocket"))
    {}

    virtual ~ServerSocket()
    {}

    ///
    /// Remove other copy/move constructors/operators
    ///
    ServerSocket(ServerSocket &) = delete;
    ServerSocket(ServerSocket &&) = delete;
    ServerSocket &operator = (ServerSocket &) = delete;
    ServerSocket &operator = (ServerSocket &&) = delete;
    
    /// 
    /// Initiate TCP listening
    /// 
    /// \throws std::logic_error when port not provided and not previously set
    /// \param backlog Number of backlog connection
    /// \param port Port number to listen to
    /// \param privKey Path to the SSL private key to use
    /// \param cert Path to the SSL cert to use
    /// \see ServerSocket::startListener
    ///
    void initServer(const unsigned int &port, const std::string &privKey = "",
        const std::string &cert = "", const std::string &host = ""
    );

    /// 
    /// Handle a client connection
    /// 
    /// \param handler ClientHandler instance to handle client connection
    /// \see ClientHandler
    ///
    void handleConnection(ClientHandler &handler);
    
protected:
    /// 
    /// Start listening for socket connection
    ///
    /// \throws logic_error if the instance is already listening for incoming
    /// connection
    /// \throws runtime_error if starting the listening fails
    /// 
    void startListener();
    
private:
    log4cplus::Logger logger;
    std::unique_ptr<SSL_CTX, std::function<void(SSL_CTX *)>> sslCtx;
    bool sslEnabled = false;

}; //class ServerSocket

} //namespace
