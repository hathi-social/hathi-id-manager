/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <utility>
#include <sstream>
#include <thread>
#include <random>
#include <algorithm>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/ndc.h>

#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

#include "clienthandler.h"

using namespace std;

namespace hathi_id_manager
{

void ClientHandler::addClient(SocketInfo && sockParam)
{
    LOG4CPLUS_DEBUG(logger, "Adding new client");
    thread(&ClientHandler::handleClient, this, move(sockParam)).detach();
}

const string ClientHandler::genSession() const
{
    random_device r;
    mt19937_64 e(r());
    normal_distribution<> dist;
    auto rnd = dist(e);

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    char *ptr = reinterpret_cast<char *>(&rnd);    string buf(ptr, 8);

    rnd = dist(e);
    buf += string(ptr, 8);
    return buf;
}

const string ClientHandler::hexEncode(const string &data) const
{
    std::ostringstream buf;
    for(unsigned char d: data)
        buf << std::setw(2) << std::setfill('0') << std::hex
            << (unsigned int)d;

    return buf.str();
}

const string ClientHandler::hexDecode(const string &data) const
{
    if(data.size() % 2)
    {
        string msg("Cannot hex-decode a string of length: ");
        msg = msg + std::to_string(data.size());
        LOG4CPLUS_ERROR(logger, msg);
        throw std::range_error(msg);
    }
    else
        LOG4CPLUS_DEBUG(logger, "String is even-length");

    const char * dptr = data.data();
    std::unique_ptr<unsigned char[]> retVal(new unsigned char[data.size()/2]);
    for(size_t i=0, j=0; i<data.size(); i+=2, j++)
    {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        string val(&dptr[i], 2);

        LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Value of val: ") << val);
        retVal[j] = static_cast<unsigned char>(stoi(val, nullptr, 16));
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    return string(reinterpret_cast<char *>(retVal.get()), data.size()/2);
}

const optional<string> ClientHandler::getSessionToken(const Json::Value &inVal,
    Json::Value &retVal) const
{
    if(!inVal.isMember("session"))
    {
        string errMsg = "\"session\" member not present";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
        return nullopt;
    }

    if(!inVal["session"].isString())
    {
        string errMsg = "\"session\" value incorrect type";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
        return nullopt;
    }

    return hexDecode(inVal["session"].asString());
}

const optional<string> ClientHandler::getActor(const Json::Value &inVal,
    Json::Value &retVal) const
{
    if(!inVal.isMember("actor"))
    {
        string errMsg = "\"actor\" member not present";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
        return nullopt;
    }

    if(!inVal["actor"].isString())
    {
        string errMsg = "\"actor\" value incorrect type";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
        return nullopt;
    }

    return inVal["actor"].asString();
}

void ClientHandler::handleClient(SocketInfo && sockParam)
{
    log4cplus::NDCContextCreator ndc(sockParam.getSocketIP());
    LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("Start handling client "));

    try
    {
        auto dbConn = initDBConn();
        Password passObj;

        Json::StreamWriterBuilder wBuilder;
        wBuilder["indentation"] = ""; // Output in one long line
        unique_ptr<Json::StreamWriter> writer(
            wBuilder.newStreamWriter()
        );

        while(keepRunning)
        {
            auto data = sockParam.readData();
            LOG4CPLUS_DEBUG(logger, "Read " << data.size() << " bytes of data");
            LOG4CPLUS_DEBUG(logger, "Data read " << data);

            if(data.size())
            {
                Json::Reader parser;
                Json::Value inVal, retVal;
                retVal["function"] = "result";
                retVal["version"] = 1;
                if(parser.parse(data, inVal))
                {
                    LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Data parsed."));
                    if(!inVal.isMember("function"))
                    {
                        string errMsg = "\"function\" member missing from client message";
                        LOG4CPLUS_INFO(logger, errMsg);
                        retVal["error"] = errMsg;
                    }
                    else if(!inVal.isMember("version"))
                    {
                        string errMsg =
                            "\"version\" member missing from client message";
                        LOG4CPLUS_INFO(logger, errMsg);
                        retVal["error"] = errMsg;
                    }
                    else if(inVal.get("version", 0).asUInt() != 1)
                    {
                        string errMsg = "Incorrect version";
                        LOG4CPLUS_INFO(logger, errMsg);
                        retVal["error"] = errMsg;
                    }
                    else if(!inVal.isMember("request"))
                    {
                        string errMsg =
                            "\"request\" member missing from client message";
                        LOG4CPLUS_INFO(logger, errMsg);
                        retVal["error"] = errMsg;
                    }
                    else
                    {
                        auto request = inVal["request"];
                        if(!request.isUInt())
                        {
                            string errMsg =
                                "\"request\" value type invalid";
                            LOG4CPLUS_INFO(logger, errMsg);
                            retVal["error"] = errMsg;
                        }
                        else
                        {
                            retVal["request"] = request;
                            auto funcName = inVal.get("function", "").asString();
                            LOG4CPLUS_TRACE(logger,
                                LOG4CPLUS_TEXT("Value of funcName: ") << funcName
                            );
                            try
                            {
                                if(funcName == "login")
                                {
                                    LOG4CPLUS_INFO(logger, "Process login request");
                                    handleLogin(*dbConn, passObj, inVal, retVal);
                                }
                                else if(funcName == "new-actor")
                                {
                                    LOG4CPLUS_INFO(logger, "Process new-identity request");
                                    handleNewActor(*dbConn, passObj, inVal, retVal);
                                }
                                else if(funcName == "delete-actor")
                                {
                                    LOG4CPLUS_INFO(logger, "Processing delete-identity request");
                                    handleDeleteActor(*dbConn, inVal, retVal);
                                }
                                else if(funcName == "logout")
                                {
                                    LOG4CPLUS_INFO(logger, "Processing logout request");
                                    handleLogout(*dbConn, inVal, retVal);
                                }
                                else if(funcName == "check-session")
                                {
                                    LOG4CPLUS_INFO(logger, "Processing check-session request");
                                    handleCheckSession(*dbConn, inVal, retVal);
                                }
                                else
                                {
                                    LOG4CPLUS_WARN(logger,
                                        LOG4CPLUS_TEXT("Unimplemented function: ") << funcName
                                    );
                                    retVal["function"] = "not-implemented";
                                }
                            }
                            catch(const exception &e)
                            {
                                string errMsg("Error processing ");
                                errMsg += funcName;
                                retVal["error"] = errMsg;
                                LOG4CPLUS_ERROR(logger,
                                    errMsg << ". Cause: " << e.what());
                            }
                        }
                    }
                }
                else
                {
                    LOG4CPLUS_ERROR(logger,
                        LOG4CPLUS_TEXT("Error parsing client message. Cause: ") <<
                            parser.getFormattedErrorMessages()
                    );
                    retVal["error"] = "Failed to parse message";
                }

                ostringstream retMsg;
                writer->write(retVal, &retMsg);
                if(sockParam.writeData(retMsg.str()) <= 0)
                {
                    LOG4CPLUS_INFO(logger,
                        LOG4CPLUS_TEXT("Lost client while sending data")
                    );
                    break;
                }
                else
                    LOG4CPLUS_DEBUG(logger, "Response sent to client");
            }
            else
            {
                LOG4CPLUS_INFO(logger,
                    LOG4CPLUS_TEXT("Lost client while reading data")
                );
                break;
            }
        }
    }
    catch(exception &e)
    {
        LOG4CPLUS_FATAL(logger,
            LOG4CPLUS_TEXT("Exception encountered handling client. Cause: ")
                << e.what()
        );
    }
}

void ClientHandler::handleLogin(DB &dbConn, Password &passObj,
    const Json::Value &inVal, Json::Value &retVal)
{
    if(!inVal.isMember("username"))
    {
        string errMsg = "\"username\" member not present";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else if(!inVal["username"].isString())
    {
        string errMsg = "\"username\" value incorrect type";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else if(!inVal.isMember("password"))
    {
        string errMsg = "\"password\" member not present";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else if(!inVal["password"].isString())
    {
        string errMsg = "\"password\" value incorrect type";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else
    {
        string username = inVal["username"].asString();
        string clearPass = inVal["password"].asString();
        auto storedPass = dbConn.getActorPass(username);
        bool auth = false;
        if(storedPass)
        {
            LOG4CPLUS_DEBUG(logger, username << " found in DB");
            auth = passObj.verifyPassword(clearPass,
                dbConn.getSalt(*storedPass), dbConn.getHashedPass(*storedPass),
                dbConn.getHashMode(*storedPass)
            );
        }
        else
            LOG4CPLUS_INFO(logger, username << " not found in DB");

        LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Login result: ") << auth);
        if(auth)
        {
            try
            {
                auto sessKey = genSession();
                auto sessStr = hexEncode(sessKey);
                dbConn.saveSession(username, sessKey);
                retVal["session"] = sessStr;
                LOG4CPLUS_INFO(logger,
                    LOG4CPLUS_TEXT("\"") << username << "\" authenticated. "
                    << "Session key: " << sessStr
                );
            }
            catch(const DBError &e)
            {
                string errMsg = "Error encountered saving session.";
                LOG4CPLUS_ERROR(logger, errMsg << " Cause: " << e.what());
                retVal["error"] = errMsg;
            }
        }
        else
        {
            LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("User ") << username <<
                " not authenticated");
            retVal["error"] = "authentication failed";
        }
    }
}

void ClientHandler::handleNewActor(DB &dbConn, Password &passObj,
    const Json::Value &inVal, Json::Value &retVal)
{
    if(!inVal.isMember("username"))
    {
        string errMsg = "\"username\" member not present";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else if(!inVal["username"].isString())
    {
        string errMsg = "\"username\" value incorrect type";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else if(!inVal.isMember("password"))
    {
        string errMsg = "\"password\" member not present";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else if(!inVal["password"].isString())
    {
        string errMsg = "\"password\" value incorrect type";
        LOG4CPLUS_INFO(logger, errMsg);
        retVal["error"] = errMsg;
    }
    else
    {
        string username = inVal["username"].asString();
        string clearPass = inVal["password"].asString();

        try
        {
            auto dgst = passObj.calcPassHash(clearPass);
            LOG4CPLUS_DEBUG(logger, "New password hashed");

            dbConn.addActor(username, passObj.getHash(dgst),
                passObj.getSalt(dgst), passObj.getHashMode(dgst));
            retVal["session"] = hexEncode(genSession());
            LOG4CPLUS_INFO(logger,
                LOG4CPLUS_TEXT("New user \"") << username << "\" saved");
        }
        catch(const DBError &e)
        {
            string errMsg = "Failed to add new identity \"" + username + "\".";
            LOG4CPLUS_WARN(logger, errMsg << " Cause: DB error, " << e.what());
            retVal["error"] = errMsg;
        }
    }
}

void ClientHandler::handleDeleteActor(DB &dbConn, const Json::Value &inVal,
    Json::Value &retVal)
{
    auto session = getSessionToken(inVal, retVal);
    if(!session)
    {
        LOG4CPLUS_DEBUG(logger, "Not processing \"delete-identity\" any further");
        return;
    }
    
    auto ident = dbConn.checkSession(session.value(), 900);
    if(!ident)
    {
        LOG4CPLUS_INFO(logger, "Session invalid");
        retVal["error"] = "session invalid";
    }
    else
    {
        auto username = ident.value();
        LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("Deleting identity \"")
            << username);
        try
        {
            if(dbConn.deleteActor(username))
            {
                LOG4CPLUS_INFO(logger, "Identity deleted");
            }
            else
            {
                LOG4CPLUS_INFO(logger, "Failed to delete identity");
                retVal["error"] = "identity delete failed";
            }
        }
        catch(const DBError &e)
        {
            LOG4CPLUS_WARN(logger,
                LOG4CPLUS_TEXT("Failed to delete identity \"")
                    << username << ". Cause: " << e.what()
            );

            // Refresh the session since the identity is still valid
            dbConn.saveSession(username, session.value());
            retVal["error"] = "identity delete failed";
        }
    }

    // Note: Do not save session. Send it back to indicate success
    retVal["session"] = hexEncode(session.value());
}

void ClientHandler::handleLogout(DB &dbConn, const Json::Value &inVal,
    Json::Value &retVal)
{
    auto session = getSessionToken(inVal, retVal);
    if(!session)
    {
        LOG4CPLUS_DEBUG(logger, "Not handling \"logout\" any further");
        return;
    }

    auto ident = dbConn.checkSession(session.value(), 900);
    if(!ident)
    {
        LOG4CPLUS_INFO(logger, "Session invalid");
        retVal["error"] = "session invalid";
    }
    else
    {
        auto username = ident.value();
        LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("Logging out user \"")
            << username << "\"");
        try
        {
            if(dbConn.deleteSession(session.value()))
            {
                LOG4CPLUS_INFO(logger, "Session deleted");
            }
            else
            {
                LOG4CPLUS_WARN(logger, "Failed to delete session");
                retVal["error"] = "Logout failed";
            }
        }
        catch(const DBError &e)
        {
            LOG4CPLUS_WARN(logger,
                LOG4CPLUS_TEXT("Failed to delete session \"")
                    << username << ". Cause: " << e.what()
            );

            // Refresh the session since the identity is still valid
            dbConn.saveSession(username, session.value());
            retVal["error"] = "Logout failed";
        }
    }

    // Note: Do not save session. Send it back to indicate success
    retVal["session"] = hexEncode(session.value());
}

void ClientHandler::handleCheckSession(DB &dbConn, const Json::Value &inVal,
    Json::Value &retVal)
{
    auto session = getSessionToken(inVal, retVal);
    if(!session)
    {
        LOG4CPLUS_DEBUG(logger, "Not handling \"check-session\" any further");
        return;
    }

    auto ident = dbConn.checkSession(session.value(), 900);
    if(!ident)
    {
        LOG4CPLUS_INFO(logger, "Session invalid");
        retVal["error"] = "session invalid";
    }
    else
    {
        LOG4CPLUS_INFO(logger, "Session valid");
        retVal["session"] = inVal["session"];
        retVal["actor"] = ident.value();

        dbConn.saveSession(ident.value(), session.value());
    }
}

} // namespace
