/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <csignal>
#include <cstring>

#include <log4cplus/configurator.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <gflags/gflags.h>

#include "idmgr-version.h"

#include "serversocket.h"
#include "clienthandler.h"
#include "pgsqldb.h"

using namespace std;
using namespace log4cplus;
using namespace hathi_id_manager;

DEFINE_string(dbhost, "localhost", "DB server host");
DEFINE_int32(dbport, 5432, "DB server port");
DEFINE_string(dbuser, "hathi_id_mgr", "DB login");
DEFINE_string(dbname, "hathi_id_mgr", "Database name");
DEFINE_string(logconfig, "", "Loging config file to pass to log4cplus");
DEFINE_uint32(port, 64756, "id-manager listening port");
DEFINE_string(privkey, "", "SSL private key");
DEFINE_string(certFile, "", "SSL server certificate");

bool keepRunning = true;
void sigHandler(int sig)
{
    Logger l = Logger::getRoot();
    switch(sig)
    {
    case SIGINT:
    case SIGTERM:
        LOG4CPLUS_DEBUG(l, "Got terminate signal");
        keepRunning = false;
        break;
    default:
        LOG4CPLUS_DEBUG(l, "Ignoring signal");
        break;
    }
}

int main(int argc, char* argv[]) 
{
    // So that --version from gflags will print the correct version info
    google::SetVersionString(version());
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    log4cplus::initialize();
    if(FLAGS_logconfig != "")
        PropertyConfigurator::doConfigure(FLAGS_logconfig);
    else
        BasicConfigurator::doConfigure();

    Logger logger = Logger::getRoot();

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
    struct sigaction action;

    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = sigHandler;
    if(sigaction(SIGINT, &action, nullptr))
    {
        int err = errno;
        LOG4CPLUS_FATAL(logger,
            LOG4CPLUS_TEXT("Failed to initialize signal handler. Cause ")
                << strerror(err)
        );
        return 1;
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-cstyle-cast)
    if(sigaction(SIGTERM, &action, nullptr))
    {
        int err = errno;
        LOG4CPLUS_FATAL(logger,
            LOG4CPLUS_TEXT("Failed to initialize signal handler. Cause ")
                << strerror(err)
        );
        return 1;
    }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-cstyle-cast)
    if(signal(SIGPIPE, SIG_IGN))
    {
        int err = errno;
        LOG4CPLUS_FATAL(logger,
            LOG4CPLUS_TEXT("Failed to initialize signal handler. Cause ")
                << strerror(err)
        );
        return 1;
    }
    
    LOG4CPLUS_INFO(logger, "Starting server");

    ServerSocket server;
    ClientHandler handler(
        [=](){
            unique_ptr<DB> db(new PgsqlDB(FLAGS_dbhost, FLAGS_dbport,
                FLAGS_dbuser, FLAGS_dbname));
            db->connect();

            return db;
        }
    );

    try
    {
        server.initServer(FLAGS_port, FLAGS_privkey, FLAGS_certFile);
    }
    catch(exception &e)
    {
        LOG4CPLUS_FATAL(logger,
            LOG4CPLUS_TEXT("Failed to start listening. Error message: ")
                << e.what()
        );
    }

    while(keepRunning)
    {
        try
        {
            server.handleConnection(handler);
        }
        catch(const bad_alloc &e)
        {
            LOG4CPLUS_FATAL(logger, "Unable to allocate memory to handle client connection");
            server.closeSocket();
            return 1;
        }
        catch(system_error &e)
        {
            LOG4CPLUS_FATAL(logger, "Failed to handle connection. Error message: "<<e.what());
            server.closeSocket();
            return 1;
        }
    }
    server.closeSocket();
	LOG4CPLUS_INFO(logger, "Server terminated");
    return 0;
}
