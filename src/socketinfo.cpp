/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <system_error>
#include <memory>
#include <functional>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>

#include <log4cplus/loggingmacros.h>
#include <openssl/err.h>

#include "socketinfo.h"

using namespace std;
using namespace log4cplus;

namespace hathi_id_manager
{

const string SocketInfo::getSocketIP() const
{
	if(!sockBio)
		throw logic_error("Address info not initialized");

    string ip;

    auto connAddr = BIO_get_conn_address(sockBio.get());
    if(connAddr)
    {
        LOG4CPLUS_TRACE(logger, "Got address from BIO");
        auto tmpHost = unique_ptr<char, function<void(char *ptr)>>(
            BIO_ADDR_hostname_string(connAddr, 1),
            [](char *ptr){ OPENSSL_free(ptr); }
        );
        auto tmpPort = unique_ptr<char, function<void(char *ptr)>>(
            BIO_ADDR_service_string(connAddr, 1),
            [](char *ptr){ OPENSSL_free(ptr); }
        );

        ip = tmpHost ? tmpHost.get() : "";
        ip += ":";
        ip += tmpPort ? tmpPort.get() : "";
    }
    else
    {
        LOG4CPLUS_TRACE(logger,
            LOG4CPLUS_TEXT("Attempt to get address from socket")
        );
        int fd;
        if(BIO_get_fd(sockBio.get(), &fd) == -1)
        {
            LOG4CPLUS_ERROR(logger,
                "Can't get file descriptor on uninitialized BIO");
        }
        else
        {
            unique_ptr<struct sockaddr> addr(
                reinterpret_cast<struct sockaddr *>(new struct sockaddr_in6)
            );
            socklen_t addrLen = sizeof(struct sockaddr_in6);
            if(getpeername(fd, addr.get(), &addrLen))
            {
                ostringstream msg;
                auto err = errno;
                msg << "Failed to get IP from file descriptor. Cause: "
                    << strerror(err);
                LOG4CPLUS_WARN(logger, msg.str());
            }
            else
            {
                char host[40];
                char port[6];
                auto ret = getnameinfo(addr.get(), addrLen, host, 40, port, 6,
                    NI_NUMERICHOST | NI_NUMERICSERV);
                if(ret)
                {
                    LOG4CPLUS_WARN(logger,
                        LOG4CPLUS_TEXT("Failed to get human-readable client IP. Cause ") <<
                        gai_strerror(ret)
                    );
                }
                else
                {
                    ip = host;
                    ip += ":";
                    ip += port;
                }
            }
        }
    }

	LOG4CPLUS_TRACE(logger, __PRETTY_FUNCTION__<<" Value of ip: "<<ip);
    return ip;
}

const string SocketInfo::readData() const
{
    const size_t dataSize = 1024;
    char data[dataSize];
    auto bio = getBio();
    auto readRslt = BIO_read(bio.get(), data, dataSize);
    if(readRslt < 0)
    {
        logOpensslError("Failed to read data", logger);
        throw runtime_error("Failed to read data");
    }
    else
        LOG4CPLUS_TRACE(logger, "read complete. Value of readRslt: " <<readRslt);

    return string(data, readRslt);
}

const size_t SocketInfo::writeData(const string &msg) const
{

    auto bio = getBio();
    auto retVal = BIO_write(bio.get(), msg.data(), msg.size());
    if(retVal < 0)
    {
        logOpensslError("Failed to write data", logger);
        throw runtime_error("Failed to write data");
    }

    BIO_flush(bio.get());
    LOG4CPLUS_TRACE(logger, "Wrote "<<retVal<<" bytes");
    return retVal;
}

void SocketInfo::logOpensslError(const string &logMsg, log4cplus::Logger logger) const
{
    const size_t bufLen = 1024;
    char buf[bufLen];
    auto errCode = ERR_get_error();
    LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Openssl err code: ") << errCode);
    ERR_error_string_n(ERR_get_error(), buf, bufLen);

    LOG4CPLUS_ERROR(logger, logMsg << " Cause: " << buf);
}

}
