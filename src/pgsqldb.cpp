/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <sstream>
#include <cstring>
#include <memory>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <log4cplus/loggingmacros.h>

#include "pgsqldb.h"

using namespace std;

namespace hathi_id_manager
{

const PgsqlDB::SocketState PgsqlDB::pollSocket(const int socket, const SocketState &watchState) const
{
    fd_set rwmask, excmask;
    FD_ZERO(&rwmask);
    FD_ZERO(&excmask);

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
    struct timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = 0;

    SocketState retVal = SocketState::UNSET;
    
    FD_SET(socket, &excmask);
    FD_SET(socket, &rwmask);
    if(watchState == SocketState::READABLE)
    {
        auto selState = select(socket+1, &rwmask, nullptr, &excmask, &tv);

        if(selState == -1)
        {
            ostringstream str;
            char buf[256];
            int err = errno;
            strerror_r(err, buf, 256);
            str << "Error polling socket. Cause: " << buf;
            throw runtime_error(str.str());
        }
        else if(selState == 0)
        {
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Socket polling timed-out"));
            retVal = SocketState::TIMEOUT;
        }
        else if(FD_ISSET(socket, &rwmask))
        {
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Socket ready for reading"));
            retVal = SocketState::READABLE;
        }
        else
            LOG4CPLUS_WARN(logger,
                LOG4CPLUS_TEXT("select returned code ") << selState);
    }
    else if(watchState == SocketState::WRITEABLE)
    {
        auto selState = select(socket+1, nullptr, &rwmask, &excmask, &tv);

        if(selState == -1)
        {
            ostringstream str;
            char buf[256];
            int err = errno;
            strerror_r(err, buf, 256);
            str << "Error polling socket. Cause: " << buf;
            throw runtime_error(str.str());
        }
        else if(selState == 0)
        {
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Socket polling timed-out"));
            retVal = SocketState::TIMEOUT;
        }
        else if(FD_ISSET(socket, &rwmask))
        {
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Socket is writable"));
            retVal = SocketState::WRITEABLE;
        }
        else
            LOG4CPLUS_WARN(logger,
                LOG4CPLUS_TEXT("select returned code ") << selState);
    }
    else
        throw range_error("Uexpected mode " + to_string(watchState));

    LOG4CPLUS_TRACE(logger,
        LOG4CPLUS_TEXT("Value to return: ") << retVal);
    return retVal;
}

void PgsqlDB::waitDBWriteable() const
{
    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Wait for DB to be writable"));
    SocketState psRslt;
    do
    {
        psRslt = pollSocket(PQsocket(conn.get()), SocketState::WRITEABLE);
        switch(psRslt)
        {
        case SocketState::TIMEOUT:
            throw DBError("Timed-out waiting for DB to be writable");
        case SocketState::WRITEABLE:
            LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("DB socket writable"));
            break;
        default:
            throw logic_error(string("Unexpected state ") + to_string(psRslt));
        }
    }while(psRslt != SocketState::WRITEABLE);

    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("DB writable"));
}

void PgsqlDB::waitResultReady() const
{
    auto flushVal = PQflush(conn.get());
    for(;;)
    {
        LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Value of flushVal: ")
            << flushVal
        );
        if(flushVal == 0)
        {
            LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Data flushed to server"));
            break;
        }
        else if(flushVal == 1)
        {
            switch(pollSocket(PQsocket(conn.get()), SocketState::READABLE))
            {
            case SocketState::TIMEOUT:
                throw DBError("Timed-out waiting to send data to server");
            case SocketState::READABLE:
                LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Socket ready for writing"));
                break;
            default:
                throw logic_error("Unexpected socket state");
            }
        }
        else
            throw DBError(
                string("Error flushing to server. Cause") 
                    + PQerrorMessage(conn.get())
            );

        flushVal = PQflush(conn.get());
    }
}

template<size_t N>
void PgsqlDB::queryDB(const string &query,
    const array<const string, N> &queryParams) const
{
    waitDBWriteable();
    LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Socket writable. Send query"));
    const size_t paramCnt = queryParams.size();
    unique_ptr<const char *[]> params(new const char *[paramCnt]);
    unique_ptr<int []> paramLens(new int[paramCnt]);

    for(unsigned int i=0; i<paramCnt; i++)
    {
        params[i] = queryParams[i].c_str();
        paramLens[i] = queryParams[i].size();
    }

    if(!PQsendQueryParams(
            conn.get(), query.c_str(), paramCnt, nullptr,
            params.get(), paramLens.get(), nullptr, 0
        )
    )
    {
        string msg("Failed to send query. Cause ");
        msg += PQerrorMessage(conn.get());
        LOG4CPLUS_ERROR(logger, msg);
        flushResponses();
        throw DBError(msg);
    }
    else
        LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Query dispatched"));

    waitResultReady();
    LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Get result"));
    if(!PQconsumeInput(conn.get()))
    {
        string msg("Failed to consume result. Cause: ");
        msg += PQerrorMessage(conn.get());
        LOG4CPLUS_ERROR(logger, LOG4CPLUS_TEXT(msg));
        flushResponses();
        throw DBError(msg);
    }
    else
        LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Result received"));
}

const string PgsqlDB::makeRawBinary(const char *escapedData) const
{
    size_t rawLength;
    auto rawBin = unique_ptr<unsigned char, function<void(void *)>>(
        PQunescapeBytea(
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            reinterpret_cast<const unsigned char*>(escapedData), &rawLength
        ),
        [](void *p){ PQfreemem(p); }
    );
    LOG4CPLUS_TRACE(logger,
        LOG4CPLUS_TEXT("Value of rawLength: ") << rawLength
    );

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    return string(reinterpret_cast<char*>(rawBin.get()), rawLength);
}

const string PgsqlDB::escapeBinary(const string &rawBin) const
{
    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Escaping binary data"));
    size_t escapedLen;
    
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    unique_ptr<unsigned char, function<void(unsigned char *)>> buf(
        PQescapeByteaConn(
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
            conn.get(), reinterpret_cast<const unsigned char *>(rawBin.data()),
            rawBin.size(), &escapedLen
        ),
        [](unsigned char *p){ PQfreemem(p); }
    );

    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Done escaping binary data"));

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    return string(reinterpret_cast<char *>(buf.get()), escapedLen);
}

void PgsqlDB::flushResponses() const
{
    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Flushing any left-over results"));
    for(;;)
    {
        try
        {
            waitResultReady();
            if(!PQconsumeInput(conn.get()))
            {
                LOG4CPLUS_WARN(logger,
                    LOG4CPLUS_TEXT("Failed to get next result for flushing. Cause ")
                    << PQerrorMessage(conn.get())
                );
                break;
            }
            auto dbRslt = PQgetResult(conn.get());
            if(!dbRslt)
            {
                LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("No more results"));
                break;
            }
            else
            {
                LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Dumping result"));
                PQclear(dbRslt);
            }
        }
        catch(exception &e)
        {
            LOG4CPLUS_WARN(logger,
                LOG4CPLUS_TEXT("Ignoring exception during flush response. ")
                << e.what()
            );
        }
    }
}

void PgsqlDB::connect()
{
    const char * const keywords[] = {
        "host",
        "port", 
        "dbname",
        "user",
        nullptr 
    };
    const char * const values[] = {
        host.c_str(),
        to_string(port).c_str(),
        dbName.c_str(),
        username.c_str(),
        nullptr
    };

    // Use a temporary; that way if the connection fails
    // PgsqlDB::conn stays null
    auto tmpConn = unique_ptr<PGconn, function<void(PGconn *)>>(
        PQconnectStartParams(keywords, values, 0),
        [](PGconn *c){ PQfinish(c); }
    );

    if(!tmpConn)
        throw runtime_error("Failed to allocate PGconn instance");

    if(PQstatus(tmpConn.get()) == CONNECTION_BAD)
    {
        string msg = "Connection failed. Cause: ";
        msg += PQerrorMessage(tmpConn.get());
        LOG4CPLUS_ERROR(logger, msg);
        throw DBError(msg);
    }

    //Covers the 1st iteration
    PostgresPollingStatusType connStage = PGRES_POLLING_WRITING;
    SocketState sockState;

    do
    {
        LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Value of connStage: ")
            << connStage
        );
        switch(connStage)
        {
        case PGRES_POLLING_WRITING:
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Waiting to write"));
            sockState = pollSocket(PQsocket(tmpConn.get()), SocketState::WRITEABLE);
            break;
        case PGRES_POLLING_READING:
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Waiting to read"));
            sockState = pollSocket(PQsocket(tmpConn.get()), SocketState::READABLE);
            break;
        case PGRES_POLLING_FAILED:
            {
                string msg = "Error connecting to the DB server. Cause: ";
                msg += PQerrorMessage(tmpConn.get());
                LOG4CPLUS_ERROR(logger, msg);
                throw DBError(msg);
            }
            break;
        default:
            LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Unchecked poll status: ")
                << connStage
            );
            break;
        }

        if(sockState == TIMEOUT)
        {
            LOG4CPLUS_ERROR(logger,
                LOG4CPLUS_TEXT("Timedout waiting for socket to be writable")
            );
            throw DBError("Timed out waiting to establish connection");
        }
        else if(sockState == WRITEABLE)
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Socket ready for writing"));
        else if(sockState == READABLE)
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Socket ready for reading"));

        LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Re-polling socket"));
        connStage = PQconnectPoll(tmpConn.get());
    } while(connStage != PGRES_POLLING_OK);
    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Connection established"));

    if(PQsetnonblocking(tmpConn.get(), 1))
    {
        LOG4CPLUS_ERROR(logger,
            LOG4CPLUS_TEXT("Failed to set connection non-blocking")
        );
        throw runtime_error("Failed to set connection non-blocking");
    }
    else
        LOG4CPLUS_TRACE(logger,
            LOG4CPLUS_TEXT("Connection set to non-blocking")
        );

    conn = move(tmpConn);
}

void PgsqlDB::addActor(const string &name, const string &hashedPass,
    const string &salt, const unsigned char &hashMode) const
{
    checkConnected();
    const array<const string, 4> queryParams = {
        name,
        escapeBinary(hashedPass),
        escapeBinary(salt),
        to_string(hashMode)
    };
    queryDB(
        "INSERT INTO hathi_actors(name, hashedpass, salt, hashmode) "
            "VALUES($1, $2, $3, $4)",
        queryParams
    );

    try
    {
        auto dbRslt = unique_ptr<PGresult, CleanPGresult>(
            PQgetResult(conn.get()), CleanPGresult()
        );
        if(PQresultStatus(dbRslt.get()) != PGRES_COMMAND_OK)
        {
            string msg("Failed to add user ");
                msg += username + ". Cause: " + PQresultErrorMessage(dbRslt.get());
            LOG4CPLUS_ERROR(logger, msg);
            throw DBError(msg);
        }
        else
            LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT(username) << " added to DB");
    }
    catch(const exception &e)
    {
        LOG4CPLUS_TRACE(logger,
            LOG4CPLUS_TEXT("Flushing responses during exception")
        );
        flushResponses();
        throw;
    }

    flushResponses();
}

const optional<const DB::password> PgsqlDB::getActorPass(const string &name) const
{
    checkConnected();
    const array<const string, 1> queryParam = { name };
    queryDB("SELECT hashedpass, salt, hashmode FROM hathi_actors WHERE name = $1",
        queryParam
    );
    optional<password> rslt;
    try
    {
        auto dbRslt = unique_ptr<PGresult, CleanPGresult>(
            PQgetResult(conn.get()), CleanPGresult()
        );
        if(PQresultStatus(dbRslt.get()) == PGRES_TUPLES_OK)
        {
            LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Results good"));
            auto cnt = PQntuples(dbRslt.get());
            if(cnt > 1)
                throw DBError(string("Query returned ") + to_string(cnt) + " rows");
            else if(cnt == 0)
            {
                LOG4CPLUS_DEBUG(logger,
                    LOG4CPLUS_TEXT("User ") << name << " not found in table"
                );
                rslt = nullopt;
            }
            else
            {
                LOG4CPLUS_DEBUG(logger,
                    LOG4CPLUS_TEXT("Actor ") << name << " found in table"
                );

                LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Get hashed password"));

                auto col = PQfnumber(dbRslt.get(), "hashedpass");
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("hashedpass column index: ") << col
                );
                
                auto fieldSize = PQgetlength(dbRslt.get(), 0, col);
                LOG4CPLUS_TRACE(logger, 
                    LOG4CPLUS_TEXT("hashedpass length: ") << fieldSize
                );
                
                auto  hashedPass = makeRawBinary(PQgetvalue(dbRslt.get(), 0, col));
                LOG4CPLUS_TRACE(logger, 
                    LOG4CPLUS_TEXT("Value of hashedpass: >>")
                    << hashedPass << "<<"
                );
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("hashedPass size: ")
                    << hashedPass.size()
                );

                LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Get salt"));

                col = PQfnumber(dbRslt.get(), "salt");
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("salt column index: ") << col
                );

                fieldSize = PQgetlength(dbRslt.get(), 0, col);
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("Salt length: ") << fieldSize
                );

                auto salt = makeRawBinary(PQgetvalue(dbRslt.get(), 0, col));
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("Value of salt: >>") << salt << "<<"
                );
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("Salt size: ") << salt.size()
                );

                LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Get hashmode"));

                col = PQfnumber(dbRslt.get(), "hashmode");
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("hashmode column index: ") << col
                );

                fieldSize = PQgetlength(dbRslt.get(), 0, col);
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("hashmode length: ") << fieldSize
                );

                unsigned char hashmode = 
                    (unsigned char)stoi(
                        string(PQgetvalue(dbRslt.get(), 0, col), fieldSize));
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("Value of hashmode: ") << hashmode
                );
                
                rslt = make_tuple(hashedPass, salt, hashmode);
            }
        }
        else
        {
            flushResponses();
            throw DBError(
                string("Error receiving result. Cause: ") +
                PQresultErrorMessage(dbRslt.get()));
        }
    }
    catch(const exception &e)
    {
        LOG4CPLUS_TRACE(logger,
            LOG4CPLUS_TEXT("Flushing responses during exception")
        );
        flushResponses();
        throw;
    }

    flushResponses();
    return rslt;
}

optional<string> PgsqlDB::checkSession(const string &session,
    const unsigned int &timeout) const
{
    optional<string> retVal = nullopt;

    checkConnected();
    array<const string, 2> queryParams = { 
        escapeBinary(session),
        to_string(timeout)
    };
    queryDB(
        "SELECT name FROM hathi_sessions "
            "WHERE session = $1 AND lastused >= NOW() - interval '1 second' * $2",
        queryParams
    );

    try
    {
        auto dbRslt = unique_ptr<PGresult, CleanPGresult>(
            PQgetResult(conn.get()), CleanPGresult()
        );
        switch(PQresultStatus(dbRslt.get()))
        {
        case PGRES_TUPLES_OK:
            LOG4CPLUS_TRACE(logger,
                LOG4CPLUS_TEXT("Results OK")
            );
            break;
        default:
            {
                string msg("Error checking session from DB. Cause: ");
                msg += PQresultErrorMessage(dbRslt.get());
                LOG4CPLUS_ERROR(logger, msg);
                throw DBError(msg);
            }
        }

        auto cnt = PQntuples(dbRslt.get());
        if(cnt > 1)
            // This is here to catch a compromised DB server that allowed multiple
            // entries for the same session token.
            throw DBError(string("Query returned ") + to_string(cnt) + " rows");
        if(cnt == 0)
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Session not in DB"));
        else
        {
            
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Session found in table"));

            auto col = PQfnumber(dbRslt.get(), "name");
            LOG4CPLUS_TRACE(logger,
                LOG4CPLUS_TEXT("name column index: ") << col
            );

            auto fieldSize = PQgetlength(dbRslt.get(), 0, col);
            LOG4CPLUS_TRACE(logger, 
                LOG4CPLUS_TEXT("name length: ") << fieldSize
            );

            retVal = string(PQgetvalue(dbRslt.get(), 0, col), fieldSize);
        }

        flushResponses();
    }
    catch(const exception &e)
    {
        LOG4CPLUS_TRACE(logger,
            LOG4CPLUS_TEXT("Cleaning up unconsumed results at exception point")
        );
        flushResponses();
        throw;
    }

    LOG4CPLUS_TRACE(logger, 
        LOG4CPLUS_TEXT("Value of retVal: ") 
        << (retVal ? retVal.value() : "null")
    );
    return retVal;
}

const bool PgsqlDB::saveSession(const string &username, const string &session) const
{
    bool retVal = false;

    checkConnected();
    auto escapedSession = escapeBinary(session);
    array<const string, 2> queryParams = {
        username,
        escapedSession
    };
    
    // See db/savesession.sql for the creation of "savesession"
    queryDB(
        "SELECT savesession($1, $2)",
        queryParams
    );

    try
    {
        auto dbRslt = unique_ptr<PGresult, CleanPGresult>(
            PQgetResult(conn.get()), CleanPGresult()
        );
        auto queryStatus = PQresultStatus(dbRslt.get());
        LOG4CPLUS_DEBUG(logger,
            LOG4CPLUS_TEXT("Query status: ") << PQresStatus(queryStatus)
        );
        if(queryStatus == PGRES_TUPLES_OK)
        {
            LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Results OK"));
            retVal = true;
        }
        else if(queryStatus == PGRES_FATAL_ERROR)
        {
            LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Encountered fatal error"));
            auto rsltField = PQresultErrorField(dbRslt.get(), PG_DIAG_CONSTRAINT_NAME);
            if(rsltField)
            {
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("Value of rsltField: ") << rsltField
                );
                if(string("hathi_sessions_username_key") == rsltField)
                {
                    LOG4CPLUS_INFO(logger,
                        LOG4CPLUS_TEXT("Session key ") << escapedSession
                            << " alread in use"
                    );
                    retVal = false;
                }
            }
            else
            {
                string msg("Error saving session. Cause: ");
                msg += PQresultErrorMessage(dbRslt.get());
                LOG4CPLUS_ERROR(logger, msg);
                throw DBError(msg);
            }
        }
        else
        {
            string msg("Error saving session. Cause: ");
            msg += PQresultErrorMessage(dbRslt.get());
            LOG4CPLUS_ERROR(logger, msg);
            throw DBError(msg);
        }
    }
    catch(const exception &e)
    {
        LOG4CPLUS_TRACE(logger, 
            LOG4CPLUS_TEXT("Cleaning up unconsumed results at exception point"));
        flushResponses();
        throw;
    }

    flushResponses();
    return retVal;
}

const bool PgsqlDB::deleteSession(const string &session) const
{
    checkConnected();
    array<const string, 1> queryParams = { escapeBinary(session) };
    queryDB(
        "DELETE FROM hathi_sessions WHERE session = $1",
        queryParams
    );

    bool retVal = false;
    try
    {
        auto dbRslt = unique_ptr<PGresult, CleanPGresult>(
            PQgetResult(conn.get()), CleanPGresult()
        );
        switch(PQresultStatus(dbRslt.get()))
        {
        case PGRES_COMMAND_OK:
        case PGRES_TUPLES_OK:
            {
                LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Results OK"));
                auto cmdTuples = PQcmdTuples(dbRslt.get());
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("Items deleted: ") << cmdTuples
                );
                if(stoi(cmdTuples) >= 1)
                {
                    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Session deleted"));
                    retVal = true;
                }
                else
                    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("No session deleted"));
            }
            break;
        default:
            {
                string msg("Error deleting session. Cause: ");
                msg += PQresultErrorMessage(dbRslt.get());
                LOG4CPLUS_ERROR(logger, msg);
                throw DBError(msg);
            }
        }
    }
    catch(const exception &e)
    {
        LOG4CPLUS_TRACE(logger,
            LOG4CPLUS_TEXT("Flushing responses during exception")
        );
        flushResponses();
        throw;
    }

    flushResponses();
    return retVal;
}

const bool PgsqlDB::deleteActor(const string &name) const
{
    checkConnected();
    array<const string, 1> queryParams = { name };
    queryDB(
        "DELETE FROM hathi_actors WHERE name = $1",
        queryParams
    );

    bool retVal = false;
    try
    {
        auto dbRslt = unique_ptr<PGresult, CleanPGresult>(
            PQgetResult(conn.get()), CleanPGresult()
        );
        switch(PQresultStatus(dbRslt.get()))
        {
        case PGRES_COMMAND_OK:
        case PGRES_TUPLES_OK:
            {
                LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Results OK"));
                auto cmdTuples = PQcmdTuples(dbRslt.get());
                LOG4CPLUS_TRACE(logger,
                    LOG4CPLUS_TEXT("Items deleted: ") << cmdTuples
                );
                if(stoi(cmdTuples) >= 1)
                {
                    LOG4CPLUS_DEBUG(logger,
                        LOG4CPLUS_TEXT("\"") << username << "\" deleted"
                    );
                    retVal = true;
                }
                else
                {
                    LOG4CPLUS_DEBUG(logger,
                        LOG4CPLUS_TEXT("No \"") << username << "\" deleted"
                    );
                }
            }
            break;
        default:
            {
                string msg("Error deleting identity. Cause: ");
                msg += PQresultErrorMessage(dbRslt.get());
                LOG4CPLUS_ERROR(logger, msg);
                throw DBError(msg);
            }
        }
    }
    catch(const exception &e)
    {
        LOG4CPLUS_TRACE(logger,
            LOG4CPLUS_TEXT("Flushing responses during exception")
        );
        flushResponses();
        throw;
    }

    flushResponses();
    return retVal;
}

} //namespace
