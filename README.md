*2019-03-01:*  
*We, the Hathi authors,  are continuing this project and claim copyright to the changes that we will be making henceforth.*  
*We will be continuing to publish under the Apache open source license.*  
*2019-02-28:*  
*As our sponsoring organisation has decided to spin down, it has been agreed to public-domain the Hathi project.*  
*Code created on or before this date is free for everyone to use and/or claim.*  

# hathi-id-manager

The module managing identification and authorization in the Hathi network.

# Project dependencies

All version are minimum version.

* cmake 3.7
* g++ 7.3
* [gflags 2.2.1](https://gflags.github.io/gflags/)
* PostgreSQL 10.6
* libpq 10.6
* [log4cplus 1.1.2](https://sourceforge.net/p/log4cplus/wiki/Home/)

On Ubuntu you can use `apt-get install g++ cmake libgflags-dev libssl-dev libpq-dev postgresql-client-10 liblog4cplus-dev libjsoncpp-dev`.

# How to run

1. Build hathi-id-manager using cmake
2. Build the database in postgres
    1. Run db/ddl.sql to create the schema
    2. Run db/acl.sql to create the roles
    3. Run db/savesession.sql to create the required store procedure
3. Create the "hathi_id_mgr" login account in Postgres
4. Add hathi_id_mgr to the hathi_id_mgr_session and hathi_id_mgr_usermgt roles
5. Start hathi-idmgr (for example ./src/hathi-idmgr). IDM will listen on port 64756 by default

# Running hathi-idmgr to communicate using TLS

Provide the private and certificate files using the "privkey" and "certFile" command-line parameters.

# Compiling to run unit test

In order to run the unit test the following environment variables must be set:

* POSTGRES_HOST
* POSTGRES_DB
* POSTGRES_USER

Set these to the DB host, db name, and db login to use. Also the .pgpass file must also have the correct entries to connect to the DB server.

# Misc notes

* hathi-idmgr.suppression is the suppression file for Valgrind
