/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gtest/gtest.h"

#include "password.h"

using namespace testing;
using namespace std;

namespace hathi_id_manager
{

TEST(PasswordTest, getMDSize)
{
    Password obj;
    EXPECT_EQ(obj.getMDSize(), 32);
    EXPECT_EQ(obj.getMDSize(1), 32);
    EXPECT_THROW({
        obj.getMDSize(2);
    }, range_error);
}

TEST(PasswordTest, calcPassHashsaltedAllBinary)
{
    Password obj;

    const unsigned char salt[] = {
        0x90,0xc9,0x5b,0x21,0xa4,0x55,0x00,0x00,0xd3,0xe0,0x68,0x09,0x7b,0x14,
        0x70,0xad,0xd5,0xc9,0x06,0xa4,0x9c,0x8c,0xe4,0x46,0x09,0xc6,0x4a,0x4b,
        0x1a,0xb6,0xf9,0xe9 };

    auto ret = obj.calcPassHash("asdf", string((char*)salt, sizeof(salt)), 1);
    const unsigned char expect[] = {
        0x27,0xeb,0xa5,0x24,0xeb,0x38,0x59,0x8b,0x8a,0x7e,0x16,0x81,0x23,
        0x30,0xe6,0x00,0xb2,0xa7,0x57,0xef,0xc8,0x33,0xea,0xcf,0xae,0x00,
        0x90,0x10,0x50,0xd9,0x26,0xa2 };

    ASSERT_EQ(obj.getSize(ret), sizeof(expect));

    string buf = obj.getHash(ret);
    const unsigned char *ptr = reinterpret_cast<const unsigned char *>(buf.data());
    EXPECT_EQ(memcmp(ptr, expect, sizeof(expect)), 0);

    EXPECT_EQ(memcmp(obj.getSalt(ret).data(), salt, 32), 0);
    EXPECT_EQ(obj.getHashMode(ret), 1);
}

TEST(PasswordTest, calcPassHashShortSalt)
{
    Password obj;

    auto ret = obj.calcPassHash("dddd", "asdf", 1);
    const unsigned char expect[] = {
        0xa0,0xde,0x0b,0x3a,0x95,0x08,0x29,0xd2,0x4e,0xea,0x47,0x1d,0x32,
        0x6e,0x64,0x76,0x21,0xf8,0x39,0x10,0x9e,0xa5,0xcd,0xe2,0x7e,0x64,
        0x01,0x95,0x1f,0xc2,0x5f,0x5c };
    
    string buf = get<0>(ret);
    const unsigned char *ptr = reinterpret_cast<const unsigned char *>(buf.data());

    ASSERT_EQ(obj.getSize(ret), sizeof(expect));
    EXPECT_EQ(obj.getSalt(ret), "asdf");
    EXPECT_EQ(memcmp(ptr, expect, sizeof(expect)), 0);
    EXPECT_EQ(obj.getHashMode(ret), 1);
}

TEST(PasswordTest, calcPassHashWrongVersion)
{
    Password obj;

    ASSERT_THROW({
        obj.calcPassHash("", "", 0);
    }, range_error);

    ASSERT_THROW({
        obj.calcPassHash("", "", 2);
    }, range_error);
}

TEST(PasswordTest, calcPassHashNoSalt)
{
    Password obj;

    auto ret = obj.calcPassHash("asdf");
    EXPECT_EQ(obj.getSize(ret), 32);
    EXPECT_EQ(obj.getHashMode(ret), 1);
    EXPECT_NE(obj.getSalt(ret).size(), 0);
}

TEST(PasswordTest, verifyPasswordValidPass)
{
    const unsigned char salt[] = {
        0x90,0xc9,0x5b,0x21,0xa4,0x55,0x00,0x00,0xd3,0xe0,0x68,0x09,0x7b,0x14,
        0x70,0xad,0xd5,0xc9,0x06,0xa4,0x9c,0x8c,0xe4,0x46,0x09,0xc6,0x4a,0x4b,
        0x1a,0xb6,0xf9,0xe9 };

    const unsigned char expect[] = {
        0x27,0xeb,0xa5,0x24,0xeb,0x38,0x59,0x8b,0x8a,0x7e,0x16,0x81,0x23,
        0x30,0xe6,0x00,0xb2,0xa7,0x57,0xef,0xc8,0x33,0xea,0xcf,0xae,0x00,
        0x90,0x10,0x50,0xd9,0x26,0xa2 };

    Password obj;
    EXPECT_TRUE(obj.verifyPassword(
        string("asdf", 4), string((char *)salt, sizeof(salt)),
        string((char *)expect, sizeof(expect)), (const unsigned char)1)
    );
}

TEST(PasswordTest, verifyPasswordInvalidPass)
{
    const unsigned char salt[] = {
        0x90,0xc9,0x5b,0x21,0xa4,0x55,0x00,0x00,0xd3,0xe0,0x68,0x09,0x7b,0x14,
        0x70,0xad,0xd5,0xc9,0x06,0xa4,0x9c,0x8c,0xe4,0x46,0x09,0xc6,0x4a,0x4b,
        0x1a,0xb6,0xf9,0xe9 };

    const unsigned char expect[] = {
        0x27,0xeb,0xa5,0x24,0xeb,0x38,0x59,0x8b,0x8a,0x7e,0x16,0x81,0x23,
        0x30,0xe6,0x00,0xb2,0xa7,0x57,0xef,0xc8,0x33,0xea,0xcf,0xae,0x00,
        0x90,0x10,0x50,0xd9,0x26,0xa2 };

    Password obj;
    EXPECT_FALSE(obj.verifyPassword(
        string("dddd", 4), string((char *)salt, sizeof(salt)),
        string((char *)expect, sizeof(expect)), (const unsigned char)1)
    );

}

TEST(PasswordTest, verifyPasswordMalformedData)
{
    const unsigned char salt[] = {
        0x90,0xc9,0x5b,0x21,0xa4,0x55,0x00,0x00,0xd3,0xe0,0x68,0x09,0x7b,0x14,
        0x70,0xad,0xd5,0xc9,0x06,0xa4,0x9c,0x8c,0xe4,0x46,0x09,0xc6,0x4a,0x4b,
        0x1a,0xb6,0xf9,0xe9 };

    const unsigned char expect[] = {
        0x27,0xeb,0xa5,0x24,0xeb,0x38,0x59,0x8b,0x8a,0x7e,0x16,0x81,0x23,
        0x30,0xe6,0x00,0xb2,0xa7,0x57,0xef,0xc8,0x33,0xea,0xcf,0xae,0x00,
        0x90,0x10,0x50,0xd9,0x26,0xa2,0xff };

    Password obj;
    EXPECT_THROW({
        obj.verifyPassword(
            string("asdf", 4), string((char *)salt, sizeof(salt)),
            string((char *)expect, sizeof(expect)), (const unsigned char)1);
    }, range_error);
}

} //namespace
