/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "clienthandler.h"
#include "mocksocketinfo.h"
#include "mockpassword.h"
#include "mockdb.h"

using namespace std;

using ::testing::Return;
using ::testing::Eq;
using ::testing::_;
using ::testing::Property;
using ::testing::Throw;
using ::testing::InvokeWithoutArgs;
using ::testing::AssertionResult;
using ::testing::AssertionSuccess;
using ::testing::AssertionFailure;

namespace hathi_id_manager
{

TEST(ClientHandlerTest, checkCommonFieldsFunctionMissing)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string("{}")))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":"
            "\"\\\"function\\\" member missing from client message\","
            "\"function\":\"result\",\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, checkCommonFieldsVersionMissing)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string("{\"function\":\"randfunc\"}")))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":"
            "\"\\\"version\\\" member missing from client message\","
            "\"function\":\"result\",\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, checkCommonFieldsWrongVersion)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(
            string("{\"function\":\"randfunc\",\"version\":0}")
        ))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":\"Incorrect version\",\"function\":\"result\",\"version\":1}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, checkCommonFieldsRequestMissing)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(
            string("{\"function\":\"randfunc\",\"version\":1}")
        ))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":"
            "\"\\\"request\\\" member missing from client message\","
            "\"function\":\"result\",\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, checkCommonFieldsReqeustWrongType)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string(
            "{\"function\":\"randfunc\",\"version\":1,\"request\":-1}"
        )))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":"
            "\"\\\"request\\\" value type invalid\","
            "\"function\":\"result\",\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, checkCommonFieldsNotImplemented)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string(
            "{\"function\":\"randfunc\",\"version\":1,\"request\":1}"
        )))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"function\":\"not-implemented\","
            "\"request\":1,\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

AssertionResult checkErrorField(const Json::Value &retVal, const string &expect)
{
    AssertionResult rslt = AssertionSuccess();
    const string field = "error";
    if(!retVal.isMember(field))
        rslt = AssertionFailure() << "\"error\" field not present";
    else if(retVal[field].asString() != expect)
        rslt = AssertionFailure() << "Incorrect \"error\" field value";
    return rslt;
}

TEST(ClientHandlerTest, hexDecodeTest)
{
    ClientHandler obj([](){ return nullptr; });
    EXPECT_THROW({ obj.hexDecode("212"); }, range_error);
    EXPECT_THROW({ obj.hexDecode("2"); }, range_error);

    {
        const unsigned char expectVal[] = { 0x21 };
        EXPECT_NO_THROW({
            EXPECT_EQ(obj.hexDecode("21"),
                string((char *)expectVal, sizeof(expectVal))
            );
        });
    }

    {
        const unsigned char expectVal[] = { 0x00, 0x11, 0x00 };
        EXPECT_NO_THROW({
            EXPECT_EQ(obj.hexDecode("001100"),
                string((char *)expectVal, sizeof(expectVal)));
        });
    }

    {
        const unsigned char expectVal[] = {   
            0x21, 0x49, 0x85, 0x1d, 0x4e, 0x3f, 0xef, 0xbf, 0x79, 0x5e,
            0xbe, 0x39, 0x9d, 0xb8, 0xe2, 0xbf 
        };
        EXPECT_NO_THROW({
            EXPECT_EQ(obj.hexDecode("2149851d4e3fefbf795ebe399db8e2bf"),
                string((char *)expectVal , sizeof(expectVal)));
        });
    }
}

TEST(ClientHandlerTest, getSessionTokenNoSession)
{
    Json::Value inVal, retVal;
    inVal["function"] = "test";
    inVal["version"] = 1;

    ClientHandler obj([](){ return nullptr; });
    auto testVal = obj.getSessionToken(inVal, retVal);
    EXPECT_FALSE(testVal);
}

TEST(ClientHandlerTest, getSessionTokenWrongType)
{
    Json::Value inVal, retVal;
    inVal["function"] = "test";
    inVal["version"] = 1;
    inVal["session"] = 234.55;

    ClientHandler obj([](){ return nullptr; });
    auto testVal = obj.getSessionToken(inVal, retVal);
    EXPECT_FALSE(testVal);
}

TEST(ClientHandlerTest, getSessionTokenValidData)
{
    const unsigned char expectVal[] = {0x00, 0x90, 0xff, 0xef};
    Json::Value inVal, retVal;
    inVal["function"] = "test";
    inVal["version"] = 1;
    inVal["session"] = "0090FFEF";

    ClientHandler obj([](){ return nullptr; });
    auto testVal = obj.getSessionToken(inVal, retVal);
    EXPECT_TRUE(testVal);
    EXPECT_EQ(memcmp(expectVal, testVal.value().data(), sizeof(expectVal)), 0);
}

TEST(ClientHandlerTest, getActorNoSession)
{
    Json::Value inVal, retVal;
    inVal["function"] = "test";
    inVal["version"] = 1;

    ClientHandler obj([](){ return nullptr; });
    auto testVal = obj.getActor(inVal, retVal);
    EXPECT_FALSE(testVal);
}

TEST(ClientHandlerTest, getActorWrongType)
{
    Json::Value inVal, retVal;
    inVal["function"] = "test";
    inVal["version"] = 1;
    inVal["actor"] = 234.55;

    ClientHandler obj([](){ return nullptr; });
    auto testVal = obj.getActor(inVal, retVal);
    EXPECT_FALSE(testVal);
}

TEST(ClientHandlerTest, getActorValidData)
{
    Json::Value inVal, retVal;
    inVal["function"] = "test";
    inVal["version"] = 1;
    inVal["actor"] = "testactor";

    ClientHandler obj([](){ return nullptr; });
    auto testVal = obj.getActor(inVal, retVal);
    EXPECT_TRUE(testVal);
    EXPECT_EQ(testVal.value(), string("testactor"));
}

TEST(ClientHandlerTest, handleLoginHandleClient)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string(
            "{\"function\":\"login\",\"version\":1,\"request\":1}"
        )))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":\"\\\"username\\\" member not present\","
            "\"function\":\"result\","
            "\"request\":1,\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, handleLoginNoUsername)
{
    MockPassword passObj;
    MockDB db;

    EXPECT_CALL(db, getActorPass(_))
        .Times(0);

    EXPECT_CALL(passObj, verifyPassword(_, _, _, _))
        .Times(0);

    Json::Value inReq, retVal;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);
    EXPECT_TRUE(checkErrorField(retVal, "\"username\" member not present"));
}

TEST(ClientHandlerTest, handleLoginUsernameWrongType)
{
    MockPassword passObj;
    MockDB db;

    EXPECT_CALL(db, getActorPass(_))
        .Times(0);

    EXPECT_CALL(passObj, verifyPassword(_, _, _, _))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["username"] = 1;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "\"username\" value incorrect type"));
}

TEST(ClientHandlerTest, handleLoginNoPassword)
{
    MockPassword passObj;
    MockDB db;

    EXPECT_CALL(db, getActorPass(_))
        .Times(0);

    EXPECT_CALL(passObj, verifyPassword(_, _, _, _))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["username"] = "user1";
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "\"password\" member not present"));
}

TEST(ClientHandlerTest, handleLoginPasswordWrongType)
{
    MockPassword passObj;
    MockDB db;

    EXPECT_CALL(db, getActorPass(_))
        .Times(0);

    EXPECT_CALL(passObj, verifyPassword(_, _, _, _))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["username"] = "user1";
    inReq["password"] = 1;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "\"password\" value incorrect type"));
}

TEST(ClientHandlerTest, handleLoginValidCredential)
{
    MockPassword passObj;
    MockDB db;

    string salt = "SALTAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    string hash = "PASSWORDAAAAAAAAAAAAAAAAAAAAAAAA";
    string username = "user1";
    string clearPass = "pass1";

    EXPECT_CALL(db, getActorPass(username))
        .WillOnce(Return(make_tuple(hash, salt, 1)));

    EXPECT_CALL(passObj, verifyPassword(clearPass, salt, hash, 1))
        .WillOnce(Return(true));

    EXPECT_CALL(db, saveSession(username, Property(&string::size, 16)))
        .Times(1);
    
    Json::Value inReq, retVal;
    inReq["username"] = username;
    inReq["password"] = clearPass;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);
    
    string expKey = "session";
    EXPECT_TRUE(retVal.isMember(expKey));

    auto session = retVal[expKey];
    EXPECT_TRUE(session.isString());
    EXPECT_EQ(session.asString().size(), 32);
}

TEST(ClientHandlerTest, handleLoginSessionSaveFail)
{
    MockPassword passObj;
    MockDB db;

    string salt = "SALTAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    string hash = "PASSWORDAAAAAAAAAAAAAAAAAAAAAAAA";
    string username = "user1";
    string clearPass = "pass1";

    EXPECT_CALL(db, getActorPass(username))
        .WillOnce(Return(make_tuple(hash, salt, 1)));

    EXPECT_CALL(passObj, verifyPassword(clearPass, salt, hash, 1))
        .WillOnce(Return(true));

    EXPECT_CALL(db, saveSession(username, Property(&string::size, 16)))
        .WillOnce(Throw(DBError("Unit test")));

    Json::Value inReq, retVal;
    inReq["username"] = username;
    inReq["password"] = clearPass;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "Error encountered saving session."));
}

TEST(ClientHandlerTest, handleLoginInvalidUser)
{
    MockPassword passObj;
    MockDB db;

    EXPECT_CALL(db, getActorPass("nouser"))
        .WillOnce(Return(nullopt));

    EXPECT_CALL(passObj, verifyPassword("nopass", _, _, 1))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["username"] = "nouser";
    inReq["password"] = "nopass";
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);

    EXPECT_FALSE(retVal.isMember("session"));
    EXPECT_TRUE(checkErrorField(retVal, "authentication failed"));
}

TEST(ClientHandlerTest, handleLoginInvalidPassword)
{
    MockPassword passObj;
    MockDB db;

    string salt = "SALTAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    string hash = "PASSWORDAAAAAAAAAAAAAAAAAAAAAAAA";
    EXPECT_CALL(db, getActorPass("user3"))
        .WillOnce(Return(make_tuple(hash, salt, 1)));

    EXPECT_CALL(passObj, verifyPassword("pass3", salt, hash, 1))
        .WillOnce(Return(false));
    
    Json::Value inReq, retVal;
    inReq["username"] = "user3";
    inReq["password"] = "pass3";
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);

    EXPECT_FALSE(retVal.isMember("session"));
    EXPECT_TRUE(checkErrorField(retVal, "authentication failed"));
}

TEST(ClientHandlerTest, handleNewActorHandleClient)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string(
            "{\"function\":\"new-actor\",\"version\":1,\"request\":1}"
        )))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":\"\\\"username\\\" member not present\","
            "\"function\":\"result\","
            "\"request\":1,\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, handleNewActorNoPassword)
{
    MockPassword passObj;
    MockDB db;

    EXPECT_CALL(db, getActorPass(_))
        .Times(0);

    EXPECT_CALL(passObj, verifyPassword(_, _, _, _))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["username"] = "user1";
    ClientHandler handler([](){ return nullptr; });
    handler.handleNewActor(db, passObj, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "\"password\" member not present"));
}

TEST(ClientHandlerTest, handleNewActorPasswordWrongType)
{
    MockPassword passObj;
    MockDB db;

    EXPECT_CALL(db, getActorPass(_))
        .Times(0);

    EXPECT_CALL(passObj, verifyPassword(_, _, _, _))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["username"] = "user1";
    inReq["password"] = 1;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogin(db, passObj, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "\"password\" value incorrect type"));
}

TEST(ClientHandlerTest, handleNewActorUserAdded)
{
    MockPassword passObj;
    MockDB db;

    string salt = "SALTAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    string hash = "PASSWORDAAAAAAAAAAAAAAAAAAAAAAAA";
    string username = "user1";
    const unsigned char hashMode = 1;
    EXPECT_CALL(passObj, calcPassHash("pass1"))
        .WillOnce(Return(tuple(hash, salt, salt.size(), hashMode)));

    EXPECT_CALL(db, addActor("user1", hash, salt, hashMode))
        .Times(1);
    
    Json::Value inReq, retVal;
    inReq["username"] = "user1";
    inReq["password"] = "pass1";
    ClientHandler handler([](){ return nullptr; });
    handler.handleNewActor(db, passObj, inReq, retVal);

    const string expKey = "session";
    EXPECT_TRUE(retVal.isMember(expKey));

    auto session = retVal[expKey];
    EXPECT_TRUE(session.isString());
    EXPECT_EQ(session.asString().size(), 32);
}

TEST(ClientHandlerTest, handleNewActorUserAddFail)
{
    MockPassword passObj;
    MockDB db;

    string salt = "SALTAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    string hash = "PASSWORDAAAAAAAAAAAAAAAAAAAAAAAA";
    string username = "user1";
    const unsigned char hashMode = 1;
    EXPECT_CALL(passObj, calcPassHash("pass1"))
        .WillOnce(Return(tuple(hash, salt, salt.size(), hashMode)));

    EXPECT_CALL(db, addActor("user1", hash, salt, hashMode))
        .WillOnce(InvokeWithoutArgs([](){ throw DBError("Unit test"); }));
    
    Json::Value inReq, retVal;
    inReq["username"] = "user1";
    inReq["password"] = "pass1";
    ClientHandler handler([](){ return nullptr; });
    handler.handleNewActor(db, passObj, inReq, retVal);
    
    EXPECT_TRUE(checkErrorField(retVal, "Failed to add new identity \"user1\"."));
}

TEST(ClientHandlerTest, handleDeleteActorHandleClient)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string(
            "{\"function\":\"delete-actor\",\"version\":1,\"request\":1}"
        )))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":\"\\\"session\\\" member not present\","
            "\"function\":\"result\","
            "\"request\":1,\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, handleDeleteActorSessionInvalid)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    MockDB db;

    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return(nullopt));

    EXPECT_CALL(db, deleteActor(_))
        .Times(0);

    EXPECT_CALL(db, saveSession(_, _))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    ClientHandler handler([](){ return nullptr; });
    handler.handleDeleteActor(db, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "session invalid"));
}

TEST(ClientHandlerTest, handleDeleteActorDeleteSuccess)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    MockDB db;

    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return("testuser"));

    EXPECT_CALL(db, deleteActor("testuser"))
        .WillOnce(Return(true));

    EXPECT_CALL(db, saveSession("testuser", sessKey))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    ClientHandler handler([](){ return nullptr; });
    handler.handleDeleteActor(db, inReq, retVal);

    string expKey = "error";
    EXPECT_FALSE(retVal.isMember(expKey));

    expKey = "session";
    EXPECT_TRUE(retVal.isMember("session"));
}

TEST(ClientHandlerTest, handleDeleteActorDeleteFail)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    MockDB db;

    const string username = "testuser";
    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return(username));

    EXPECT_CALL(db, deleteActor(username))
        .WillOnce(InvokeWithoutArgs([]()->bool{ throw DBError("Unit test"); }));

    EXPECT_CALL(db, saveSession(username, sessKey))
        .Times(1);

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    ClientHandler handler([](){ return nullptr; });
    handler.handleDeleteActor(db, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "identity delete failed"));

    string expKey = "session";
    EXPECT_TRUE(retVal.isMember(expKey));

    auto session = retVal[expKey];
    EXPECT_TRUE(session.isString());
    EXPECT_EQ(session.asString(), sessKeyStr);
}

TEST(ClientHandlerTest, handleLogoutHandleClient)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string(
            "{\"function\":\"logout\",\"version\":1,\"request\":1}"
        )))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":\"\\\"session\\\" member not present\","
            "\"function\":\"result\","
            "\"request\":1,\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, handleLogoutSessionInvalid)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    MockDB db;

    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return(nullopt));

    EXPECT_CALL(db, deleteSession(sessKey))
        .Times(0);

    EXPECT_CALL(db, saveSession(sessKey, _))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogout(db, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "session invalid"));
}

TEST(ClientHandlerTest, handleLogoutSuccess)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    MockDB db;

    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return("testuser"));

    EXPECT_CALL(db, deleteSession(sessKey))
        .WillOnce(Return(true));

    EXPECT_CALL(db, saveSession("testuser", sessKey))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogout(db, inReq, retVal);

    string expKey = "error";
    EXPECT_FALSE(retVal.isMember(expKey));

    expKey = "session";
    EXPECT_TRUE(retVal.isMember("session"));
}

TEST(ClientHandlerTest, handleLogoutFail)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    MockDB db;

    const string username = "testuser";
    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return(username));

    EXPECT_CALL(db, deleteSession(sessKey))
        .WillOnce(InvokeWithoutArgs([]()->bool{ throw DBError("Unit test"); }));

    EXPECT_CALL(db, saveSession(username, sessKey))
        .Times(1);

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    ClientHandler handler([](){ return nullptr; });
    handler.handleLogout(db, inReq, retVal);

    EXPECT_TRUE(checkErrorField(retVal, "Logout failed"));

    string expKey = "session";
    EXPECT_TRUE(retVal.isMember(expKey));

    auto session = retVal[expKey];
    EXPECT_TRUE(session.isString());
    EXPECT_EQ(session.asString(), sessKeyStr);
}

TEST(ClientHandlerTest, handleCheckSessionHandleClient)
{
    MockSocketInfo socket;

    EXPECT_CALL(socket, getSocketIP())
        .WillOnce(Return(string("127.0.0.1:12345")));

    EXPECT_CALL(socket, readData())
        .WillOnce(Return(string(
            "{\"function\":\"check-session\",\"version\":1,\"request\":1}"
        )))
        .WillOnce(Return(string()));

    string expectMsg =
        "{\"error\":\"\\\"session\\\" member not present\","
            "\"function\":\"result\","
            "\"request\":1,\"version\":1"
        "}";
    EXPECT_CALL(socket, writeData(expectMsg))
        .WillOnce(Return(expectMsg.size()));

    ClientHandler obj([](){ return nullptr; });
    obj.keepRunning=true;
    obj.handleClient(move(socket));
}

TEST(ClientHandlerTest, handleCheckSessionValidSession)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    const string expUsername = "testuser";
    MockDB db;

    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return(expUsername))
        .WillOnce(Return(expUsername));

    EXPECT_CALL(db, saveSession(expUsername, sessKey))
        .WillOnce(Return(true))
        .WillOnce(Return(true));

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    ClientHandler handler([](){ return nullptr; });
    handler.handleCheckSession(db, inReq, retVal);

    EXPECT_TRUE(retVal.isMember("session")
        && retVal["session"].asString() == sessKeyStr);
    EXPECT_TRUE(retVal.isMember("actor")
        && retVal["actor"].asString() == "testuser"
    );
    EXPECT_FALSE(retVal.isMember("error"));

    EXPECT_EQ(retVal["session"].asString(), sessKeyStr) << "Failed on 1st pass";

    handler.handleCheckSession(db, inReq, retVal);
    EXPECT_TRUE(retVal.isMember("session")
        && retVal["session"].asString() == sessKeyStr);
    EXPECT_TRUE(retVal.isMember("actor")
        && retVal["actor"].asString() == "testuser"
    );
    EXPECT_FALSE(retVal.isMember("error"));

    EXPECT_EQ(retVal["session"].asString(), sessKeyStr) << "Failed on 2nd pass";
}

TEST(ClientHandlerTest, handleCheckSessionInvalidSession)
{
    const unsigned char rawSessKey[] = { 0x00, 0x10, 0x50 };
    const string sessKey((char *)rawSessKey, 3);
    const string sessKeyStr = "001050";

    const string expUsername = "testuser";
    MockDB db;

    EXPECT_CALL(db, checkSession(sessKey, _))
        .WillOnce(Return(nullopt));

    EXPECT_CALL(db, saveSession(expUsername, sessKeyStr))
        .Times(0);

    Json::Value inReq, retVal;
    inReq["session"] = sessKeyStr;
    inReq["actor"] = "otheruser";
    ClientHandler handler([](){ return nullptr; });
    handler.handleCheckSession(db, inReq, retVal);

    EXPECT_FALSE(retVal.isMember("session"));
    EXPECT_TRUE(checkErrorField(retVal, "session invalid"));
}

} // namespace
