/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdexcept>

#include <gtest/gtest.h>

#include "pgsqldb.h"
#include "cliparams.h"

using namespace std;

namespace hathi_id_manager
{

TEST(PgsqlDBTest, connectSuccess)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    ASSERT_NO_THROW({
        obj.connect();
    });
    ASSERT_NE(obj.conn, nullptr);
}

TEST(PgsqlDBTest, connectFail)
{

    PgsqlDB obj("localhost", 5442, "hathi_unittest", "hathi_id_mgr_unittest");
    ASSERT_THROW({
        obj.connect();
    }, DBError);

    ASSERT_EQ(obj.conn, nullptr);
}

TEST(PgsqlDBTest, getActorPassNotConnected)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);

    EXPECT_THROW({ obj.getActorPass("user1"); }, logic_error);
}

TEST(PgsqlDBTest, getActorPassSuccess)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char expectSalt[] = {
        0x90,0xc9,0x5b,0x21,0xa4,0x55,0x00,0x00,0xd3,0xe0,0x68,0x09,0x7b,0x14,
        0x70,0xad,0xd5,0xc9,0x06,0xa4,0x9c,0x8c,0xe4,0x46,0x09,0xc6,0x4a,0x4b,
        0x1a,0xb6,0xf9,0xe9 };

    const unsigned char expectHash[] = {
        0x27,0xeb,0xa5,0x24,0xeb,0x38,0x59,0x8b,0x8a,0x7e,0x16,0x81,0x23,
        0x30,0xe6,0x00,0xb2,0xa7,0x57,0xef,0xc8,0x33,0xea,0xcf,0xae,0x00,
        0x90,0x10,0x50,0xd9,0x26,0xa2 };

    ASSERT_NO_THROW({ obj.connect(); });

    optional<DB::password> data;
    ASSERT_NO_THROW({ data = obj.getActorPass("user1"); });
    EXPECT_TRUE(data);

    auto hash = obj.getHashedPass(data.value());
    EXPECT_EQ(hash.size(), sizeof(expectHash));
    EXPECT_EQ(memcmp(hash.data(), expectHash, sizeof(expectHash)), 0);

    auto salt = obj.getSalt(data.value());
    EXPECT_EQ(salt.size(), sizeof(expectSalt));
    EXPECT_EQ(memcmp(salt.data(), expectSalt, sizeof(expectSalt)), 0);

    EXPECT_EQ(obj.getHashMode(data.value()), 1);
    ASSERT_NO_THROW({ data = obj.getActorPass("user100"); });
    EXPECT_FALSE(data);
}

TEST(PgsqlDBTest, addActorNotConnected)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);

    const unsigned char hashedPass[] = {
        0x20,0x57,0xa0,0x6e,0x67,0xbe,0xd2,0xe3,0xdc,0x96,0x05,0xbe,0xd8,0xa9,
        0x52,0xcb,0x8f,0xc7,0x2b,0x28,0x49,0xa2,0x8a,0xa6,0x71,0x75,0x71,0xee,
        0x95,0x9a,0xd9,0x3d
    };
    const unsigned char salt[] = {
        0xbd,0xdc,0x4b,0x03,0x27,0x91,0xca,0x52,0x43,0xb8,0xc1,0x2e,0x92,0x2f,
        0xab,0x0b,0x4e,0x35,0x22,0xcb,0x5e,0xef,0x62,0xdd,0x06,0xfd,0x5f,0x6a,
        0x1b,0x1c,0x6a,0x16
    };
    const unsigned char mode = 1;

    ASSERT_THROW(
        {
            obj.addActor("user3", string((char *)hashedPass, sizeof(hashedPass)),
                string((char *)salt, sizeof(salt)), mode);
        },
        logic_error
    );
}

TEST(PgsqlDBTest, addActorSuccess)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);

    const unsigned char hashedPass[] = {
        0x20,0x57,0xa0,0x6e,0x67,0xbe,0xd2,0xe3,0xdc,0x96,0x05,0xbe,0xd8,0xa9,
        0x52,0xcb,0x8f,0xc7,0x2b,0x28,0x49,0xa2,0x8a,0xa6,0x71,0x75,0x71,0xee,
        0x95,0x9a,0xd9,0x3d
    };
    const unsigned char salt[] = {
        0xbd,0xdc,0x4b,0x03,0x27,0x91,0xca,0x52,0x43,0xb8,0xc1,0x2e,0x92,0x2f,
        0xab,0x0b,0x4e,0x35,0x22,0xcb,0x5e,0xef,0x62,0xdd,0x06,0xfd,0x5f,0x6a,
        0x1b,0x1c,0x6a,0x16
    };
    const unsigned char mode = 1;

    ASSERT_NO_THROW({ obj.connect(); });

    EXPECT_NO_THROW({
        obj.addActor(
            "user3", string((char *)hashedPass, sizeof(hashedPass)),
            string((char *)salt, sizeof(salt)), mode
        );
    });

    auto data = obj.getActorPass("user3").value();
    EXPECT_EQ(obj.getHashMode(data), mode);

    auto savedHash = obj.getHashedPass(data);
    EXPECT_EQ(savedHash.size(), sizeof(hashedPass));
    EXPECT_EQ(
        memcmp(savedHash.data(), hashedPass, sizeof(hashedPass)), 0
    );
    
    auto savedSalt = obj.getSalt(data);
    EXPECT_EQ(
        memcmp(savedSalt.data(), salt, sizeof(salt)), 0
    );
}

TEST(PgsqlDBTest, checkSessionNotConnected)
{
    const unsigned char tmp[] = { 0xaa,0xbb,0xcc };
    string session((char *)tmp, sizeof(tmp));

    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    EXPECT_THROW({
        obj.checkSession(session, 10); 
    }, logic_error);    
}

TEST(PgsqlDBTest, checkSessionShortSession)
{
    const unsigned char tmp[] = { 0xaa,0xbb,0xcc };
    string session((char *)tmp, sizeof(tmp));

    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    ASSERT_NO_THROW({ obj.connect(); });
    EXPECT_NO_THROW({
        auto rslt = obj.checkSession(session,10);
        EXPECT_FALSE(rslt);
    });
}

TEST(PgsqlDBTest, checkSessionLongSession)
{
    const unsigned char tmp[] = {
        0xAA,0xBB,0xCC,0xDD,0xEE,0xFF,0x00,0x11,0x22,0x33,0x44,0x55,0x66,
        0x77,0x88,0x99
    };
    string session((char *)tmp, sizeof(tmp));

    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    ASSERT_NO_THROW({ obj.connect(); });
    EXPECT_NO_THROW({
        // Expectation here is tests/unit/pgsql.sql is loaded within 2
        // minutes before the unit test is ran
        auto rslt = obj.checkSession(session,120);
        EXPECT_TRUE(rslt);
        EXPECT_EQ(rslt.value(), string("user1"));
    });
}

TEST(PgsqlDBTest, saveSessionNotConnected)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char tmp[] = { 0xaa,0xbb,0xcc };
    string session((char*)tmp, sizeof(tmp));
    string username("savesessionuser");

    EXPECT_THROW({
        obj.saveSession(username, session);
    }, logic_error);
}

TEST(PgsqlDBTest, saveSessionNewSession)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char tmp[] = { 0xaa,0xbb,0xcc };
    string session((char*)tmp, sizeof(tmp));
    string username("savesessionuser");

    ASSERT_NO_THROW({ obj.connect(); });

    // New session
    EXPECT_NO_THROW({
        EXPECT_TRUE(
            obj.saveSession(username, session)
        );
    });

    auto rslt = obj.checkSession(session, 120);
    EXPECT_NO_THROW({ EXPECT_EQ(rslt.value(), username); });
}

TEST(PgsqlDBTest, saveSessionResetSession)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char tmp[] = { 0xaa,0xbb,0xcc };
    string session((char*)tmp, sizeof(tmp));
    string username("savesessionuser");

    ASSERT_NO_THROW({ obj.connect(); });

    // New session
    EXPECT_NO_THROW({
        EXPECT_TRUE(
            obj.saveSession(username, session)
        );
    });

    // Reset session time
    EXPECT_NO_THROW({
        EXPECT_TRUE(
            obj.saveSession(username, session)
        );
    });

    {
        auto rslt = obj.checkSession(session, 2);
        EXPECT_NO_THROW({ EXPECT_EQ(rslt.value(), username); });
    }
}

TEST(PgsqlDBTest, saveSessionReuseSession)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char tmp[] = { 0xaa,0xbb,0xcc };
    string session((char*)tmp, sizeof(tmp));
    string username("savesessionuser");

    ASSERT_NO_THROW({ obj.connect(); });

    // New session
    EXPECT_NO_THROW({
        EXPECT_TRUE(
            obj.saveSession(username, session)
        );
    });

    // Test reusing active session
    EXPECT_NO_THROW({
        EXPECT_FALSE(
            obj.saveSession("user2", session)
        );
    });
}

TEST(PgsqlDBTest, saveSessionChangeSession)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char tmp[] = { 0xaa,0xbb,0xcc };
    string session((char*)tmp, sizeof(tmp));
    string username("savesessionuser");

    ASSERT_NO_THROW({ obj.connect(); });

    // New session
    EXPECT_NO_THROW({
        EXPECT_TRUE(
            obj.saveSession(username, session)
        );
    });

    // Test new session value for user1
    {
        const unsigned char tmp[] = { 0xee,0xff,0x10 };
        string session2((char *)tmp, sizeof(tmp));
        EXPECT_NO_THROW({
            EXPECT_TRUE(obj.saveSession(username, session2));
        });

        EXPECT_FALSE(obj.checkSession(session, 120));
        auto rslt = obj.checkSession(session2, 120);
        EXPECT_NO_THROW({ EXPECT_EQ(rslt.value(), username); });
    }
}

TEST(PgsqlDBTest, saveSessionNonCharData)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned int session3 = 500;
    
    ASSERT_NO_THROW({ obj.connect(); });
    EXPECT_NO_THROW({
        obj.saveSession("user2", string((char *)&session3, sizeof(session3)));
    });
}

TEST(PgsqlDBTest, deleteSessionNotConnected)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char tmp[] = {
        0xDD,0xDD,0xDD,0xDD,0xDD,0xDE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEA,0xAA,
        0x12,0x34
    };
    string session((char *)tmp, sizeof(tmp));

    EXPECT_THROW({
        obj.deleteSession(session);
    }, logic_error);
}

TEST(PgsqlDBTest, deleteSessionSuccess)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);
    const unsigned char tmp[] = {
        0xDD,0xDD,0xDD,0xDD,0xDD,0xDE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEA,0xAA,
        0x12,0x34
    };
    string session((char *)tmp, sizeof(tmp));

    ASSERT_NO_THROW({ obj.connect(); });

    ASSERT_NO_THROW({
        EXPECT_TRUE(obj.deleteSession(session));
        EXPECT_FALSE(obj.checkSession(session, 900));
    });

    ASSERT_NO_THROW({ EXPECT_FALSE(obj.deleteSession(session)); });

    const unsigned int tmp2 = 42;
    string sess2(reinterpret_cast<const char *>(&tmp2), sizeof(tmp2));
    ASSERT_NO_THROW({ EXPECT_FALSE(obj.deleteSession(sess2)); });
}

TEST(PgsqlDBTest, deleteActor)
{
    PgsqlDB obj(FLAGS_dbhost, FLAGS_dbport, FLAGS_dbuser, FLAGS_dbname);

    EXPECT_THROW({
        obj.deleteActor("del_actor_test");
    }, logic_error);

    ASSERT_NO_THROW({ obj.connect(); });

    EXPECT_NO_THROW({
        EXPECT_TRUE(obj.deleteActor("del_actor_test"));
        auto rsltUser = obj.getActorPass("del_actor_test");
        EXPECT_FALSE(rsltUser);
    });

    EXPECT_NO_THROW({
        EXPECT_FALSE(obj.deleteActor("del_actor_test"));
    });

    EXPECT_NO_THROW({
        EXPECT_FALSE(obj.deleteActor("del_actor_test2"));
    });
}

} //namespace
