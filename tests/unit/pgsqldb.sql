TRUNCATE TABLE hathi_actors CASCADE;

INSERT INTO hathi_actors(name, salt, hashedpass, hashmode)
VALUES ('user1',
    '\x90c95b21a4550000d3e068097b1470add5c906a49c8ce44609c64a4b1ab6f9e9',
    '\x27eba524eb38598b8a7e16812330e600b2a757efc833eacfae00901050d926a2',
    1),
    ('user2', '\x000001', '\x000001',1),
    ('savesessionuser', '\x00', '\x00', 1),
    ('actor_test', '\x00', '\x00', 1),
    ('save_actor_test', '\x00', '\x00', 1),
    ('del_actor_test', '\x00', '\x00', 1),
    ('del_session', '\x00', '\x00', 1);

INSERT INTO hathi_sessions(name, session)
VALUES('user1', '\xAABBCCDDEEFF00112233445566778899'),
    ('del_session', '\xDDDDDDDDDDDEEEEEEEEEEEEEEAAA1234');
